artifactory_url="http://localhost:8081/artifactory"
artifactory_url="http://127.0.0.1:8081/artifactory"


repo="libs-snapshot-local"
artifacts="com/qaagility/javaee/HelloWorldJavaEE"
name=$artifact

url=$artifactory_url/$repo/$artifacts
echo "URL   ="+$url

version=`curl -s $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`
build=`curl -s $url/$version/maven-metadata.xml | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`
BUILD_LATEST="$url/$version/HelloWorldJavaEE-$build.war"

echo $BUILD_LATEST > filename.txt